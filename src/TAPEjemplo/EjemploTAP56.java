/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TAPEjemplo;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author merarimaysosa
 */
public class EjemploTAP56 {
    public static void main(String[] args) {
        EjemploTAP56 instancia = new EjemploTAP56();
        
        instancia.run();
        
    }

    private void run() {
        JFrame ventana = new JFrame();
        
        ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //para que se libere todos los recursos cuando cierre la ventana. Se cierra o termina el programa
        ventana.setSize(400,300);
        
        ventana.setLayout(new BorderLayout());//objetos que se encargar de organizar los componentes dentro de una ventana. Los acomoda al tamaño
        //Existen varios tipos de layout, border layout, box layout, etc
        
        JLabel etiqueta = new JLabel("Hola Mundo");
        JButton boton = new JButton("Cerrar");
        
        boton.addActionListener(new MiControlador());//añadir una accion al boton (metodo 1) clase anonima
        
        // se define en que parte de la ventana va el boton BorderLayout.
        ventana.add(etiqueta, BorderLayout.CENTER); //para que se vean en la ventana
        ventana.add(boton, BorderLayout.PAGE_END);//para que se vean en la ventana
        
        ventana.setVisible(true);//para que se vea la ventana
    }
}

class MiControlador implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
    JOptionPane.showMessageDialog(null, "Hola mundo, otra vez!. Presionaron el boton cerrar.");
        }
    
}
