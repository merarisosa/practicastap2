package practica4;

// events --> key
//keyPressed(KeyEvent e): Se ejecuta cuando el usuario presiona una tecla.
//keyReleased(KeyEvent e): Se ejecuta cuando el usuario libera una tecla
//keyTyped(KeyEvent e): Se ejecuta cuando el usuario presiona una tecla, pero solo cuando la tecla corresponde a caractéres, teclas especiales como F1, F2 entre otras no son identificadas.

public class P4Imitador extends javax.swing.JFrame {
    
    public P4Imitador() {
        initComponents();
        
        bGOriginal.add(rButtonOriginal1);
        bGOriginal.add(rButtonOriginal2);
        bGOriginal.add(rButtonOriginal3);
        
        bGEspejo.add(rButtonEspejo1);
        bGEspejo.add(rButtonEspejo2);
        bGEspejo.add(rButtonEspejo3);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bGOriginal = new javax.swing.ButtonGroup();
        bGEspejo = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        rButtonOriginal1 = new javax.swing.JRadioButton();
        rButtonOriginal2 = new javax.swing.JRadioButton();
        rButtonOriginal3 = new javax.swing.JRadioButton();
        rButtonOriginal4 = new javax.swing.JCheckBox();
        rButtonOriginal5 = new javax.swing.JCheckBox();
        rButtonOriginal6 = new javax.swing.JCheckBox();
        txtOriginal = new javax.swing.JTextField();
        comboBoxOriginal = new javax.swing.JComboBox<>();
        spinnerOriginal = new javax.swing.JSpinner();
        rButtonEspejo1 = new javax.swing.JRadioButton();
        rButtonEspejo2 = new javax.swing.JRadioButton();
        rButtonEspejo3 = new javax.swing.JRadioButton();
        rButtonEspejo4 = new javax.swing.JCheckBox();
        rButtonEspejo5 = new javax.swing.JCheckBox();
        rButtonEspejo6 = new javax.swing.JCheckBox();
        txtEspejo = new javax.swing.JTextField();
        spinnerEspejo = new javax.swing.JSpinner();
        comboBoxEspejo = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Original");

        jLabel2.setText("Espejo");

        rButtonOriginal1.setText("Opcion 1");
        rButtonOriginal1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rButtonOriginal1ActionPerformed(evt);
            }
        });

        rButtonOriginal2.setText("Opcion 2");
        rButtonOriginal2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rButtonOriginal2ActionPerformed(evt);
            }
        });

        rButtonOriginal3.setText("Opcion 3");
        rButtonOriginal3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rButtonOriginal3ActionPerformed(evt);
            }
        });

        rButtonOriginal4.setText("Opcion 4");
        rButtonOriginal4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rButtonOriginal4ActionPerformed(evt);
            }
        });

        rButtonOriginal5.setText("Opcion 5");
        rButtonOriginal5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rButtonOriginal5ActionPerformed(evt);
            }
        });

        rButtonOriginal6.setText("Opcion 6");
        rButtonOriginal6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rButtonOriginal6ActionPerformed(evt);
            }
        });

        txtOriginal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOriginalActionPerformed(evt);
            }
        });
        txtOriginal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtOriginalKeyReleased(evt);
            }
        });

        comboBoxOriginal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboBoxOriginal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxOriginalActionPerformed(evt);
            }
        });

        spinnerOriginal.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerOriginalStateChanged(evt);
            }
        });

        rButtonEspejo1.setText("Opcion 1");
        rButtonEspejo1.setEnabled(false);
        rButtonEspejo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rButtonEspejo1ActionPerformed(evt);
            }
        });

        rButtonEspejo2.setText("Opcion 2");
        rButtonEspejo2.setEnabled(false);

        rButtonEspejo3.setText("Opcion 3");
        rButtonEspejo3.setEnabled(false);

        rButtonEspejo4.setText("Opcion 4");
        rButtonEspejo4.setEnabled(false);

        rButtonEspejo5.setText("Opcion 5");
        rButtonEspejo5.setEnabled(false);

        rButtonEspejo6.setText("Opcion 6");
        rButtonEspejo6.setEnabled(false);

        txtEspejo.setEnabled(false);
        txtEspejo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEspejoActionPerformed(evt);
            }
        });

        spinnerEspejo.setEnabled(false);

        comboBoxEspejo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboBoxEspejo.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1))
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rButtonOriginal3)
                    .addComponent(rButtonOriginal1)
                    .addComponent(rButtonOriginal2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rButtonOriginal6)
                    .addComponent(rButtonOriginal4)
                    .addComponent(rButtonOriginal5))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtOriginal)
                    .addComponent(comboBoxOriginal, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(spinnerOriginal))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rButtonEspejo3)
                            .addComponent(rButtonEspejo2)
                            .addComponent(rButtonEspejo1))
                        .addGap(63, 63, 63)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rButtonEspejo4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(rButtonEspejo5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(rButtonEspejo6, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(42, 42, 42)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtEspejo)
                            .addComponent(comboBoxEspejo, 0, 142, Short.MAX_VALUE)
                            .addComponent(spinnerEspejo))))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rButtonOriginal1)
                    .addComponent(rButtonOriginal4)
                    .addComponent(txtOriginal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rButtonOriginal2)
                    .addComponent(rButtonOriginal5)
                    .addComponent(comboBoxOriginal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rButtonOriginal3)
                    .addComponent(rButtonOriginal6)
                    .addComponent(spinnerOriginal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rButtonEspejo1)
                    .addComponent(rButtonEspejo4)
                    .addComponent(txtEspejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rButtonEspejo2)
                    .addComponent(rButtonEspejo5)
                    .addComponent(comboBoxEspejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rButtonEspejo3)
                    .addComponent(rButtonEspejo6)
                    .addComponent(spinnerEspejo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(59, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rButtonOriginal3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rButtonOriginal3ActionPerformed
        // TODO add your handling code here:
        boolean rbEspejo3 = rButtonOriginal3.isSelected();
        rButtonEspejo3.setSelected(rbEspejo3);
    }//GEN-LAST:event_rButtonOriginal3ActionPerformed

    private void comboBoxOriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxOriginalActionPerformed
        // TODO add your handling code here:
        int item2 = comboBoxOriginal.getSelectedIndex();
        comboBoxEspejo.setSelectedIndex(item2);
    }//GEN-LAST:event_comboBoxOriginalActionPerformed

    private void txtOriginalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOriginalActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_txtOriginalActionPerformed

    private void rButtonEspejo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rButtonEspejo1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rButtonEspejo1ActionPerformed

    private void txtEspejoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEspejoActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_txtEspejoActionPerformed

    private void txtOriginalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOriginalKeyReleased
        String txt = txtOriginal.getText();
        txtEspejo.setText(txt);
    }//GEN-LAST:event_txtOriginalKeyReleased

    private void spinnerOriginalStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerOriginalStateChanged
        // TODO add your handling code here:
        spinnerEspejo.setValue(spinnerOriginal.getValue());
    }//GEN-LAST:event_spinnerOriginalStateChanged

    private void rButtonOriginal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rButtonOriginal1ActionPerformed
        // TODO add your handling code here:
        boolean rbEspejo1 = rButtonOriginal1.isSelected();
        rButtonEspejo1.setSelected(rbEspejo1);
    }//GEN-LAST:event_rButtonOriginal1ActionPerformed

    private void rButtonOriginal2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rButtonOriginal2ActionPerformed
        // TODO add your handling code here:
        boolean rbEspejo2 = rButtonOriginal2.isSelected();
        rButtonEspejo2.setSelected(rbEspejo2);
    }//GEN-LAST:event_rButtonOriginal2ActionPerformed

    private void rButtonOriginal4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rButtonOriginal4ActionPerformed
        // TODO add your handling code here:
        boolean rCBOriginal4 = rButtonOriginal4.isSelected();
        rButtonEspejo4.setSelected(rCBOriginal4);
    }//GEN-LAST:event_rButtonOriginal4ActionPerformed

    private void rButtonOriginal5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rButtonOriginal5ActionPerformed
        // TODO add your handling code here:
        boolean rCBOriginal5 = rButtonOriginal5.isSelected();
        rButtonEspejo5.setSelected(rCBOriginal5);
    }//GEN-LAST:event_rButtonOriginal5ActionPerformed

    private void rButtonOriginal6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rButtonOriginal6ActionPerformed
        // TODO add your handling code here:
        boolean rCBOriginal6 = rButtonOriginal6.isSelected();
        rButtonEspejo6.setSelected(rCBOriginal6);
    }//GEN-LAST:event_rButtonOriginal6ActionPerformed
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(P4Imitador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(P4Imitador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(P4Imitador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(P4Imitador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new P4Imitador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bGEspejo;
    private javax.swing.ButtonGroup bGOriginal;
    private javax.swing.JComboBox<String> comboBoxEspejo;
    private javax.swing.JComboBox<String> comboBoxOriginal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rButtonEspejo1;
    private javax.swing.JRadioButton rButtonEspejo2;
    private javax.swing.JRadioButton rButtonEspejo3;
    private javax.swing.JCheckBox rButtonEspejo4;
    private javax.swing.JCheckBox rButtonEspejo5;
    private javax.swing.JCheckBox rButtonEspejo6;
    private javax.swing.JRadioButton rButtonOriginal1;
    private javax.swing.JRadioButton rButtonOriginal2;
    private javax.swing.JRadioButton rButtonOriginal3;
    private javax.swing.JCheckBox rButtonOriginal4;
    private javax.swing.JCheckBox rButtonOriginal5;
    private javax.swing.JCheckBox rButtonOriginal6;
    private javax.swing.JSpinner spinnerEspejo;
    private javax.swing.JSpinner spinnerOriginal;
    private javax.swing.JTextField txtEspejo;
    private javax.swing.JTextField txtOriginal;
    // End of variables declaration//GEN-END:variables
}
