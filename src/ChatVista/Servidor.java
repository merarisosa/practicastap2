package ChatVista;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class Servidor {

    //arreglos creados para almacenar los datos correspondientes a 
    //conexiones y lkos usuarios que se encuentran activos
    
    ArrayList<Conexion> conexiones;
    ArrayList<String> usersOnline = new ArrayList<String>();
    
    //socket del servidor para el intercambio de datos 
    //flujo de datos en el socket 
    ServerSocket ss;
    
    //mencion de los usuarios existentes para accesar al chat
    String[][] usuarios = {
        {"hugo", "123"},
        {"paco", "456"},
        {"pedro", "789"},
        {"luis", "890"},
        {"donald", "678"}};

    private static final String MESSAGE_SERVER = "message_server";
    private static final String MESSAGE_LOGIN = "message_login";
    private static final String MESSAGE = "message";
    private static final String USERS_ONLINE = "users_online";

    ;
    
    //en el main se inicia el servidor a través del método run
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).iniciarServidor();
            }
        }
        );
    }
    
    
 //inicia el sevidor en espera de conexiones 
    private void iniciarServidor() {
        
        this.conexiones = new ArrayList<>();
        Socket socket;
        
//se ejecuta el bloque try catch para iniciar el servidor 
        Conexion cnx;

        //try ss con el puerto 4444
        try {
            ss = new ServerSocket(4444);
            System.out.println("Servidor iniciado, en espera de conexiones entrantes...");

            //mientras sea asi, se acepta la conexión y se inicializa
            while (true) {
                socket = ss.accept();
                cnx = new Conexion(this, socket, conexiones.size());
                conexiones.add(cnx);
                cnx.start();
            }
            
//bloque catch por defecto ioe para entradaa y salida del programa 
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // se hace la difusión de mensajes, acceso en el chat para enviar mensajes
    
    private void bcChat(String id, String mensaje, String type, String user, Boolean all) {
        Conexion hilo;
        JSONObject message = new JSONObject();
        message.put("type", type);
        message.put("message", mensaje);
        message.put("sender", user);
        
        if (all) {
            for (int i = 0; i < this.conexiones.size(); i++) {
                hilo = this.conexiones.get(i);
                if (hilo.cnx.isConnected() && hilo.estado == hilo.CHAT) {
                    hilo.enviar(message.toString());
                }
            }
        } else {
            for (int i = 0; i < this.conexiones.size(); i++) {
                hilo = this.conexiones.get(i);
                if (hilo.cnx.isConnected() && hilo.estado == hilo.CHAT && !id.equals(hilo.id)) {
                    hilo.enviar(message.toString());
                }
            }
        }
    }

    private void muestraUsersOnline(String id) {
        Conexion hilo;
        JSONObject message = new JSONObject();
        message.put("type", USERS_ONLINE);
        JSONArray a = new JSONArray(usersOnline);
        message.put("Usuarios en línea", a);
        for (int i = 0; i < this.conexiones.size(); i++) {
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected() && hilo.estado == hilo.CHAT) {
                hilo.enviar(message.toString());
            }
        }
    }

    class Conexion extends Thread {

        public final int SIN_USER = 0;
        public final int USER_IDENT = 1;
        public final int PASS_PDTE = 2;
        public final int PASS_OK = 3;
        public final int CHAT = 4;
        int numCnx = -1;
        
        //leer texto en el flujo de datos, entrada 
        BufferedReader in;
        
        //crea y escribe - imprime los datos, salida 
        PrintWriter out;
        
        Socket cnx;
        Servidor padre;
        
        //establece un estado inicial para campos
        int estado = SIN_USER;
        String id = "";

       //metodo para la conexion con el servidor 
        public Conexion(Servidor padre, Socket socket, int num) {
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress() + num;
        }

        @Override
        public void run() {
            String linea = "", username = "", pass = "";
            JSONObject user, message;
            int usr = -1;
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(), true);
                System.out.printf("Aceptando conexion desde %s\n", cnx.getInetAddress().getHostAddress());
                while (estado != CHAT) {
                    switch (estado) {
                        case SIN_USER:
                            statusLogin("Bienvenido!, introduzca sus datos", false);
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            linea = in.readLine();
                            user = new JSONObject(linea);
                            if (user.has("username") && user.has("password")) {
                                username = user.getString("username");
                                pass = user.getString("password");
                                boolean found = false;
                                for (int i = 0; i < usuarios.length; i++) {
                                    if (username.equals(usuarios[i][0]) && pass.equals(usuarios[i][1])) {
                                        found = true;
                                        usr = i;
                                    }
                                }
                                if (!found) {
                                    statusLogin("El usuario o la contraseña es incorrecto, intente nuevamente", false);
                                } else {
                                    estado = CHAT;
                                    statusLogin("Inicio de sesión exitoso", true);
                                }
                            }
                            break;
                    }
                }
                this.padre.bcChat(id, username + " se ha conectado al chat", MESSAGE_SERVER, username, true);
                usersOnline.add(username);
                this.padre.muestraUsersOnline(id);
                System.out.printf("[%s] %s esta online \n", cnx.getInetAddress().getHostAddress(), username);
                while ((linea = in.readLine()) != null) {
                    message = new JSONObject(linea);
                    String type = message.getString("type");
                    if (type.equals(MESSAGE)) {
                        
                        //mensaje para recepción de salida del chat
                        //abandona el chat de manera segura
                        
                        if (!message.getString("message").equals("exit()")) {
                            System.out.printf("[%s] %s: %s\n", cnx.getInetAddress().getHostAddress(), username, linea);
                            this.padre.bcChat(this.id, message.getString("message"), MESSAGE, username, false);
                        } else {
                            this.padre.bcChat(id, username + " ha salido del chat. Hasta luego", MESSAGE_SERVER, username, true);
                            System.out.printf("[%s] %s se ha desconectado del servidor\n", cnx.getInetAddress().getHostAddress(), username);
                            estado = SIN_USER;
                            usersOnline.remove(username);
                            this.padre.muestraUsersOnline(id);
                            break;
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void enviar(String mensaje) {
            this.out.println(mensaje);
        }

        private void statusLogin(String mensaje, boolean online) {
            JSONObject message = new JSONObject();
            message.put("type a message", MESSAGE_LOGIN);
            message.put("online status", online);
            message.put("message on screen", mensaje);
            enviar(message.toString());
        }
    }
}
